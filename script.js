var tipsJohn = [];
var paidAmountJohn = [];
var familyJohn = {
    restaurant : [124,48,268,180,42],
    tipCalculator : function(){
        for (i = 0; i < this.restaurant.length ; i++){
            if(this.restaurant[i] < 50){
               tipsJohn.push(this.restaurant[i] * 0.2);
               paidAmountJohn.push((this.restaurant[i] * 0.2) + this.restaurant[i]);
            }
            else if(this.restaurant[i] >= 50 && this.restaurant[i] <= 200){
                tipsJohn.push(this.restaurant[i] * 0.15);
                paidAmountJohn.push((this.restaurant[i] * 0.2) + this.restaurant[i]);
            }
            else{
                tipsJohn.push(this.restaurant[i] * 0.1);
                paidAmountJohn.push((this.restaurant[i] * 0.2) + this.restaurant[i]);
            }
        }
    }
};

var tipsMark = [];
var paidAmountMark = [];
var familyMark = {
    restaurant : [77,5,110,45],
    tipCalculator : function(){
        for (i = 0; i < this.restaurant.length ; i++){
            if(this.restaurant[i] < 100){
               tipsMark.push(this.restaurant[i] * 0.2);
               paidAmountMark.push((this.restaurant[i] * 0.2) + this.restaurant[i]);
            }
            else if(this.restaurant[i] >= 100 && this.restaurant[i] <= 300){
                tipsMark.push(this.restaurant[i] * 0.1);
                paidAmountMark.push((this.restaurant[i] * 0.1) + this.restaurant[i]);
            }
            else{
                tipsMark.push(this.restaurant[i] * 0.25);
                paidAmountMark.push((this.restaurant[i] * 0.25) + this.restaurant[i]);
            }
        }
    }
};

familyJohn.tipCalculator();
familyMark.tipCalculator();

var calcAverage = function(tips){
    var sum = 0
    for (var i = 0; i < tips.length; i++){
        sum = sum + tips[i];
    }
    return sum / tips.length;
}

familyJohn.averageTips = calcAverage(tipsJohn);
familyMark.averageTips = calcAverage(tipsMark);

console.log(familyMark.averageTips,familyJohn.averageTips);

if(familyJohn.averageTips > familyMark.averageTips){
    console.log('John\'s Average is greater than Mark\'s Average');
}
else if(familyJohn.averageTips < familyMark.averageTips){
    console.log('Mark\'s Average is greater than John\'s Average');
}
else{
    console.log('John and Mark have the same Average');
}
